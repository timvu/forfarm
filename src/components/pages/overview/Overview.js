import React from 'react'
import GoldCard from "../../goldcard/GoldCard";
import './overview.css';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const MULES = [
    {
        username: "email@email.com",
        password: "mypassword",
        proxy: "192.168.1.1:8000",
        startTime: new Date(),
        gold: 500*1000,
        inGame: true,
        world: 103
    }
];

class Overview extends React.Component {

    render() {

        return (
            <div className="overview">
                <div className="cards">
                    <GoldCard title="Session earnings" amount={500 * 1000}/>
                    <GoldCard title="Current gold" amount={25 * 1000 * 1000}/>
                    <GoldCard title="Hourly earnings" amount={5 * 1000 * 1000}/>
                </div>
                <div className="instances">
                    <TableContainer component={Paper} color="inherit">
                        <Toolbar>
                            <Typography variant="h6">Mules</Typography>
                        </Toolbar>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Username</TableCell>
                                    <TableCell>Password</TableCell>
                                    <TableCell>Proxy</TableCell>
                                    <TableCell>Start Time</TableCell>
                                    <TableCell>Gold</TableCell>
                                    <TableCell>In game</TableCell>
                                    <TableCell>World</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    MULES.map(m => (
                                        <TableRow>
                                            <TableCell>{m.username}</TableCell>
                                            <TableCell>{m.password}</TableCell>
                                            <TableCell>{m.proxy}</TableCell>
                                            <TableCell>{m.startTime.toLocaleTimeString()}</TableCell>
                                            <TableCell>{m.gold}</TableCell>
                                            <TableCell>{m.inGame.toString()}</TableCell>
                                            <TableCell>{m.world}</TableCell>
                                        </TableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
                <div className="instances">
                    <TableContainer component={Paper}>
                        <Toolbar>
                            <Typography variant="h6">Bots</Typography>
                        </Toolbar>
                        <Table className="table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Username</TableCell>
                                    <TableCell>Password</TableCell>
                                    <TableCell>Proxy</TableCell>
                                    <TableCell>Start Time</TableCell>
                                    <TableCell>Gold</TableCell>
                                    <TableCell>In game</TableCell>
                                    <TableCell>World</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    MULES.map(m => (
                                        <TableRow>
                                            <TableCell>{m.username}</TableCell>
                                            <TableCell>{m.password}</TableCell>
                                            <TableCell>{m.proxy}</TableCell>
                                            <TableCell>{m.startTime.toLocaleTimeString()}</TableCell>
                                            <TableCell>{m.gold}</TableCell>
                                            <TableCell>{m.inGame.toString()}</TableCell>
                                            <TableCell>{m.world}</TableCell>
                                        </TableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
            </div>
        )
    }
}

export default Overview;