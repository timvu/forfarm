import React from 'react'
import Header from '../header/Header';
import SideBar from '../sidebar/SideBar';
import Overview from "../pages/overview/Overview";
import './App.css'

const App = () => {

    return (
        <div className="page">
            <div className="logo">
            </div>
            <Header></Header>
            <SideBar/>
            <Overview></Overview>
        </div>
    );
};

export default App