import React from 'react';
import './SideBar.css'

class SideBar extends React.Component {

    render () {
        return (
            <nav>
                <ul>
                    <li>Overview</li>
                    <li>Accounts</li>
                    <li>Proxies</li>
                    <li>Log</li>
                    <li>Settings</li>
                </ul>
            </nav>
        );
    }
}

export default SideBar;