import {
    GET_ACCOUNTS,
    UPDATE_ACCOUNT,
    REMOVE_ACCOUNT
} from "./types";

export const getAccounts = () => {
    return {
        type: GET_ACCOUNTS,
        payload: []
    }
};

