import {
    GET_ACCOUNTS,
    UPDATE_ACCOUNT,
    REMOVE_ACCOUNT
} from "../actions/types";


//TODO: finish reducer
export default (state = [], action) => {

    switch(action.type){
        case GET_ACCOUNTS:
            return action.payload;
        case UPDATE_ACCOUNT:
            return []
        case REMOVE_ACCOUNT:
            return []
        default:
            return []
    }

};